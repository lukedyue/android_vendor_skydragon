# BusyBox
PRODUCT_PACKAGES += \
    busybox

# CAF Telephony packages
PRODUCT_PACKAGES += \
    ims-ext-common \
    telephony-ext
 PRODUCT_BOOT_JARS += \
    telephony-ext

# Extra packages
PRODUCT_PACKAGES += \
    Launcher3 \
    Launcher3QuickStep \
    OmniStyle \
    OmniJaws \
    Phonograph \
    Stk \
    Terminal

# Fonts
PRODUCT_PACKAGES += \
    SkyDragonFonts

# HDK-Mod
PRODUCT_PACKAGES += \
    HDK-Mod

# Spectrum
PRODUCT_PACKAGES += \
    Spectrum

# Google Settings Intelligence
#PRODUCT_PACKAGES += \
#    SettingsIntelligenceGooglePrebuilt

# Turbo
PRODUCT_PACKAGES += \
    Turbo
