# Accents
PRODUCT_PACKAGES += \
    AccentHotPink \
    AccentPixel \
    AccentYellow \
    AccentDeepOrange \
    AccentOmni \
    AccentWhite \
    AccentTeal \
    AccentLightBlue \
    AccentGreen \
    AccentDeepPurple \
    AccentCerulean \
    AccentEmerald \
    AccentLime \
    AccentMaroon \
    AccentPumpkin \
    AccentPink \
    AccentCrimson \
    AccentBlack \
    AccentAmber \
    AccentGrey \
    AccentFuchsia \
    AccentGreenTea \
    AccentOctane \
    AccentPlum \
    AccentNuclearSlime \
    AccentBeach

# Notifications
PRODUCT_PACKAGES += \
    NotificationsDark \
    NotificationsLight \
    NotificationsPrimary

# Primary Colors
PRODUCT_PACKAGES += \
    PrimaryAlmostBlack \
    PrimaryBlack \
    PrimaryOmni \
    PrimaryWhite \
    PrimaryDarkBlue \
    PrimaryPlum 

# App Themes
PRODUCT_PACKAGES += \
    DocumentsUITheme \
    DialerTheme \
    TelecommTheme \
    PhonographTheme
